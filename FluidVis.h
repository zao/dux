#pragma once

#include "FluidSim.h"

struct SDL_Window;
struct SDL_Surface;

class FluidVis {
	FluidVis& operator = (FluidVis const&);
	FluidVis(FluidVis const&);

public:
	FluidVis();
	~FluidVis();

	void beginFrame();
	void endFrame();

	void draw(FluidSim const& Sim);

private:
	SDL_Window* Window = nullptr;
	SDL_Surface* ScreenSurface = nullptr;
	SDL_Surface* BlipSurface = nullptr;
};