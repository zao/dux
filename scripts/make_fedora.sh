#!/usr/bin/env bash

CPPFLAGS="-Wall -std=c++14"
CXXFLAGS=""
LIBS="-lSDL2 -lSDL2_image"
g++ $CPPFLAGS $CXXFLAGS -o bin/dux *.cc $LIBS