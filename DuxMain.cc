#include <iostream>

#include <stdint.h>
#include "FluidSim.h"
#include "FluidVis.h"
#include "VectorMath.h"

#include <SDL2/SDL.h>

int main() {
	FluidVis Vis;
	FluidSim Sim;
	Sim.setExtents({-10.0f, -10.0f, -10.0f}, {10.0f, 10.0f, 10.0f});
	Sim.setDeltaTime(1.0f, 100.0f);
	Sim.populate(100);
	SDL_Event Event;

	uint32_t SimTic = SDL_GetTicks();
	uint32_t TicksPerSimStep = (uint32_t)(Sim.getDeltaTime() * 1000.0f);
	uint32_t MaxSimTicksPerFrame = 4;

	while (1) {
		bool KeepRunning = true;
		while (SDL_PollEvent(&Event)) {
			if (Event.type == SDL_KEYUP && Event.key.keysym.sym == SDLK_ESCAPE) {
				KeepRunning = false;
			}
			if (Event.type == SDL_QUIT) {
				KeepRunning = false;
			}
		}
		if (!KeepRunning) break;
		uint32_t Now = SDL_GetTicks();
		uint32_t SimTicksThisFrame = 0;
		if (SimTic + TicksPerSimStep <= Now) {
			Sim.step();
			SimTic += TicksPerSimStep;
			if (++SimTicksThisFrame == MaxSimTicksPerFrame) {
				SimTic += (Now - SimTic) / TicksPerSimStep;
			}
		}

		Vis.beginFrame();
		Vis.draw(Sim);
		Vis.endFrame();
	}
	return 0;
}
