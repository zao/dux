#include "FluidVis.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

FluidVis::FluidVis() {
	SDL_Init(SDL_INIT_EVERYTHING);
	IMG_Init(IMG_InitFlags::IMG_INIT_PNG);
	Window = nullptr;
	ScreenSurface = nullptr;
	Window = SDL_CreateWindow("dux", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1024, 1024, SDL_WINDOW_BORDERLESS);
	ScreenSurface = SDL_GetWindowSurface(Window);
	BlipSurface = IMG_Load("blip.png");
}

FluidVis::~FluidVis() {
	SDL_FreeSurface(BlipSurface);
	SDL_DestroyWindow(Window);
	IMG_Quit();
	SDL_Quit();
}

void FluidVis::beginFrame() {
	uint32_t ClearColor = SDL_MapRGB(ScreenSurface->format, 0x20, 0x40, 0x60);
	SDL_FillRect(ScreenSurface, nullptr, ClearColor);
}

void FluidVis::endFrame() {
	SDL_UpdateWindowSurface(Window);
}

SDL_Rect centerRectAround(SDL_Rect R, float3 C) {
	SDL_Rect Ret = R;
	Ret.x = (int)(C.x - R.w / 2.0f);
	Ret.y = (int)(C.y - R.h / 2.0f);
	return Ret;
}

void FluidVis::draw(FluidSim const& Sim) {
	float3 ScreenOffset{512.0f, 512.0f, 0.0f};
	for (auto& Position : Sim.Positions) {
		float3 ScreenPos = float3{0.0f, 10.0f, 0.0f} - Position * 10.0f + ScreenOffset;
		SDL_Rect DstRect = centerRectAround(BlipSurface->clip_rect, ScreenPos);
		SDL_BlitSurface(BlipSurface, nullptr, ScreenSurface, &DstRect);
	}
}