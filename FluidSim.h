#pragma once
#include "VectorMath.h"
#include <memory>
#include <vector>

class FluidSim {
	float DT;
public:
	float3 MinP, MaxP;
	std::vector<float3> Positions;

	float getDeltaTime() const;

	void setDeltaTime(float Numerator, float Denominator);
	void setExtents(float3 Mins, float3 Maxes);
	void populate(size_t Count);

	void step();
};
