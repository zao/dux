#include "FluidSim.h"
#include <stddef.h>
#include <stdint.h>
#include <random>

static void swap(float& A, float& B) {
	float V = A;
	A = B;
	B = V;
}

float FluidSim::getDeltaTime() const {
	return DT;
}

void FluidSim::setDeltaTime(float Numerator, float Denominator) {
	DT = Numerator / Denominator;
}

static float clamp(float V, float LowBound, float HighBound) {
	float Ret = V;
	if (Ret < LowBound) {
		Ret = LowBound;
	}
	else if (Ret > HighBound) {
		Ret = HighBound;
	}
	return Ret;
}

static float3 clampPosition(float3 Pos, float3 MinP, float3 MaxP) {
	float3 Ret;
	Ret.x = clamp(Pos.x, MinP.x, MaxP.x);
	Ret.y = clamp(Pos.y, MinP.y, MaxP.y);
	Ret.z = clamp(Pos.z, MinP.z, MaxP.z);
	return Ret;
}

static void reorder(float& A, float& B) {
	if (A > B) swap(A, B);
}

void FluidSim::setExtents(float3 Mins, float3 Maxes) {
	reorder(Mins.x, Maxes.x);
	reorder(Mins.y, Maxes.y);
	reorder(Mins.z, Maxes.z);
	MinP = Mins;
	MaxP = Maxes;

	for (auto& Pos : Positions) {
		clampPosition(Pos, MinP, MaxP);
	}
}

void FluidSim::populate(size_t Count) {
	using UniformFloat = std::uniform_real_distribution<float>;
	static std::mt19937 RNG(9001u);
	UniformFloat XDist(MinP.x, MaxP.x);
	UniformFloat YDist(MinP.y, MaxP.y);
	UniformFloat ZDist(MinP.z, MaxP.z);
	for (auto I = 0u; I < Count; ++I) {
		float3 Pos;
		Pos.x = XDist(RNG);
		Pos.y = YDist(RNG);
		Pos.z = ZDist(RNG);
		Positions.emplace_back(std::move(Pos));
	}
}

struct Poly6Kernel {
	static float w(float3 Ra, float3 Rb, float H) {
		float const R2 = length2(Ra - Rb);
		if (R2 > H*H) return 0.0f;

		float const H9 = pow(H, 9.0f);
		float Ret = 315.0f / (64.0f * pi * H9) * pow(H*H - R2, 3.0f);
		return Ret;
	}
};

struct SpikyKernel {
	static float w(float3 Ra, float3 Rb, float H) {
		float const R1 = length(Ra - Rb);
		if (R1 > H) return 0.0f;
		
		float const H6 = pow(H, 6.0f);
		float Ret = 15.0f / (pi * H6) * pow(H - R1, 3.0f);
		return Ret;
	}
};

struct ViscosityKernel {
	static float w(float3 Ra, float3 Rb, float H) {
		float const R1 = length(Ra - Rb);
		if (R1 > H) return 0.0f;
		
		float const R2 = R1*R1;
		float const R3 = R1*R1*R1;
		float const H2 = H*H; 
		float const H3 = H*H*H;
		float Ret = 15.0f / (2.0f * pi * H3) * (-R3/(2*H3) + R2/H2 + H/(2*R1) - 1);
		return Ret;
	}
};

void FluidSim::step() {
	float3 const G{0.0f, -9.81f, 0.0f};
	size_t const N = Positions.size();
	for (size_t Idx = 0; Idx < N; ++Idx) {
		float3 force = G;
		auto Pos = Positions[Idx] + DT * force;
		Pos = clampPosition(Pos, MinP, MaxP);
		Positions[Idx] = Pos;
	}
}