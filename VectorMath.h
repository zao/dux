#pragma once

#include <math.h>

namespace { float const pi = 3.14159265359; }

struct float3 {
	union {
		struct {
			float x, y, z;
		};
		float v[3];
	};
};

struct float4 {
	union {
		struct {
			float x, y, z, w;
		};
		float v[4];
	};
};

inline float3& operator += (float3& lhs, float3 rhs) {
	lhs.x += rhs.x;
	lhs.y += rhs.y;
	lhs.z += rhs.z;
	return lhs;
}

inline float3 operator + (float3 lhs, float3 rhs) {
	auto ret = lhs;
	ret += rhs;
	return ret;
}

inline float3 operator -= (float3& lhs, float3 rhs) {
	lhs.x -= rhs.x;
	lhs.y -= rhs.y;
	lhs.z -= rhs.z;
	return lhs;
}

inline float3 operator - (float3 lhs, float3 rhs) {
	auto ret = lhs;
	ret -= rhs;
	return ret;
}

inline float3& operator *= (float3& lhs, float rhs) {
	lhs.x *= rhs;
	lhs.y *= rhs;
	lhs.z *= rhs;
	return lhs;
}

inline float3 operator * (float3 lhs, float rhs) {
	auto ret = lhs;
	ret *= rhs;
	return ret;
}

inline float3 operator * (float lhs, float3 rhs) {
	auto ret = rhs;
	ret *= lhs;
	return ret;
}

inline float3& operator /= (float3& lhs, float rhs) {
	lhs.x /= rhs;
	lhs.y /= rhs;
	lhs.z /= rhs;
	return lhs;
}

inline float3 operator / (float3 lhs, float rhs) {
	auto ret = lhs;
	ret /= rhs;
	return ret;
}

inline float dot(float3 lhs, float3 rhs) {
	auto ret = lhs.x*rhs.x + lhs.y*rhs.y + lhs.z*rhs.z;
	return ret;
}

inline float length(float3 v) {
	auto ret = sqrt(dot(v, v));
	return ret;
}

inline float length2(float3 v) {
	auto ret = fabs(dot(v, v));
	return ret;
}

inline float distance(float3 a, float3 b) {
	auto ret = length(a - b);
	return ret;
}

inline float distance2(float3 a, float3 b) {
	auto ret = length2(a - b);
	return ret;
}